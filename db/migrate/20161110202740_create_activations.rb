class CreateActivations < ActiveRecord::Migration[5.0]
  def change
    create_table :activations do |t|
      t.string :customer_phone_number
      t.string :customer_name
      t.string :dealer_store
      t.string :dealer_number
      t.string :device_id
      t.string :activation_code
      t.integer :zip_code
      t.integer :area_code
      t.string :number_to_port_in
      t.string :account_number
      t.string :account_passcode

      t.timestamps
    end
  end
end
