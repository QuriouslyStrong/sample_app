class AddDealerStoreToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :dealer_store, :string
  end
end
