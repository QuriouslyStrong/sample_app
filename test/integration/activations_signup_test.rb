require 'test_helper'

class ActivationsSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information" do
    get activations_path
    assert_no_difference 'Activation.count' do
      post activations_path, params: { activation: { dealer_store:  "",
                                         customer_name: "user@invalid",
                                         device_id: "foo",
                                         activation_code: "bar",
                                         zip_code: "1234567890",
                                         area_code: "Wei Mobile",
                                         number_to_port_in: "1111111111",
                                         account_number: "1111111111",
                                         account_passcode: "1234"   } }
    end
    assert_template 'activations/new'
  end
end