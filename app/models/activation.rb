class Activation < ApplicationRecord
  validates :dealer_store, presence: true, length: { maximum: 50 }
  validates :customer_name, presence: true, length: { maximum: 50 }
  validates :device_id, presence: true, length: { maximum: 20 }
  validates :activation_code, presence: true, length: { maximum: 20 }
  validates :zip_code, presence: true, length: { maximum: 5 }
  validates :area_code, presence: true, length: { maximum: 3 }
  #validates :number_to_port_in, presence: true, length: { maximum: 10 }
  #validates :account_number, presence: true, length: { maximum: 20 }
  #validates :account_passcode, presence: true, length: { maximum: 10 }
  
end

