class ActivationsController < ApplicationController
  
  def show
    @activation = Activation.find(params[:id])
  end
  def new
    @activation = Activation.new
  end
  def create
    @activation = Activation.new(activation_params)
    if @activation.save
      redirect_to @activation
    else
      render 'new'
    end
  end
  
  private
  
  def activation_params
    params.require(:activation).permit(:dealer_store, :customer_name, :device_id, :activation_code, :zip_code, :area_code)
    
end
end