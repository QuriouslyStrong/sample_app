Rails.application.routes.draw do
  
  get 'sessions/new'

  get 'users/new'
  get 'activations/new'

  root :to => 'static_pages#home'
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  get '/activations', to: 'activations#new'
  post '/activations', to: 'activations#create'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users
  resources :activations
  
end
